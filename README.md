# tew-0x04

just an ultrasimple laravel mvc demo for *tew* classes

### commands
run commands directly in the container (which is named `laravel`) using `docker-compose exec`

#### create a new controller

```bash
$ docker-compose exec laravel php artisan make:controller CarsController --invokable
```

#### create a migration

```bash
$ docker-compose exec laravel php artisan make:migration create_cars_table
```

#### perform predefined migrations

```bash
$ docker-compose exec laravel php artisan migrate
```

## first setup

### docker

running on `fedora 32` via `moby-engine`

```bash
$ sudo  dnf install moby-engine moby-engine-vim moby-engine-zsh-completion \
  && sudo systemctl enable --now docker
```

enable [backward compatibility](https://fedoraproject.org/wiki/Common_F31_bugs#Docker_package_no_longer_available_and_will_not_run_by_default_.28due_to_switch_to_cgroups_v2.29) for Cgroups using `systemd.unified_cgroup_hierarchy=0` kernel arg (reboot implied), otherwise docker won't function correctly

make sure your user is in the `docker` group to be able to use `docker`/`docker-compose`<br>commands without `sudo`

run the following and log out/back in to apply the change

```bash
$ sudo groupadd docker
$ sudo usermod -aG docker $USER
```

why to use docker

* the development environment setup process is greatly simplified thanks to using a `docker-compose` magic

* can use mere `docker-compose up` to start up the container bundle

the `docker-compose.yml` file is largely based on the [bitnami one](https://hub.docker.com/r/bitnami/laravel), it was appropriated, though, to play better with fedora

### selinux

after some hours of despair and a fair amount of `selinux` sorcery I was finally able to connect to the database (YMMV), you may need to push `docker` to the *permissive* zone

### firewall

try enabling docker0 (docker networking interface) in the firewall first, running as root

```bash
$ sudo firewall-cmd --permanent --zone=trusted --change-interface=docker0
```

followed by 

```bash
$ sudo firewall-cmd --reload
```

and restart docker with

```bash
$ sudo systemctl restart docker
```

**Note:** restarting docker will **stop** any running containers

if the above still doesn't allow you to connect to the database, also allow the docker's bridge interface in the firewall in a similar manner
