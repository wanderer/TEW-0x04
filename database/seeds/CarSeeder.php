<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
            'brand'=>'Audi',
            'model'=>'A6',
            'production_y'=> Carbon::now(),
        ]);
        DB::table('cars')->insert([
            'brand'=>'Skoda',
            'model'=>'Superb',
            'production_y'=> Carbon::now(),
        ]);
        DB::table('cars')->insert([
            'brand'=>'BMW',
            'model'=>'M5',
            'production_y'=> Carbon::now(),
        ]);
    }
}
