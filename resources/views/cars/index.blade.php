<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie-edge">
    <title>Car index</title>
</head>
<body>
    <h1>All cars</h1>
    <p>Tu su novinky<p>
    <dl>
        @foreach ($cars as $car)
            <dt>{{ $car->brand }}</dt>
            <dd>{{ $car->model }} ({{ $car->production_y }})</dd>
        @endforeach
    </dl>
</body>
</html>
