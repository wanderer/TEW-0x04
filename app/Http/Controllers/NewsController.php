<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
    }

    public function index() {
        $brand = ['audi','bmw','ferari'];
        return view('REALnews.index', ['brand'=>$brand]);
        // echo 'NewsController:index';
    }
}
